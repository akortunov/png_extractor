require 'bindata'
require 'zlib'
require 'getoptlong'
require 'fileutils'

#Windows version

USAGE_SHORT = "\
Try to use: png_extractor_win [-h|--help] for more information"
USAGE = "\
    Usage:  png_extractor_win [-h|--help] [options] [file(s)]
      -d, --directory 	DIR	Path to extract data (./extracted by default)
      -c, --crc       		Only extract chunks with CRC32-mismatch
      -e, --end      		Only extract data next for IEND chunk
      -t, --type	REG	Processing only chunks of type REG (may use regexp)
      -n, --no-extract		Only show data
      -u, --unknown   		Only extract chunks of unknown type

      This script requires a 'bindata' gem. You can install it by command:
    # gem install bindata"
    
chunk_types=["IHDR", "PLTE", "IDAT", "IEND", "bKGD", "cHRM", "gAMA", "hIST", "iCCP", "iTXt",
                 "pHYs", "sBIT", "sPLT", "sRGB", "sTER", "tEXt", "tIME", "tRNS", "zTXT"]

class PngHeader < BinData::Record
        endian :big
        string :header, :read_length => 8
end

class PngChunk < BinData::Record
        endian :big
        uint32 :len
        string :type, :read_length => 4
        string :data, :read_length => :len
        uint32 :checksum
end 
    
def extract_chunk(chunk, i, dir)
	filename = dir + "/" + chunk.type + i.to_s# + "-" + chunk.checksum.to_s
	f = open(filename, 'wb')
	f.write(chunk.len.to_binary_s+chunk.type+chunk.data)
	f.close
end
    
begin
 opts = GetoptLong.new(['--help',       '-h', GetoptLong::NO_ARGUMENT],
                       ['--directory',  '-d', GetoptLong::OPTIONAL_ARGUMENT],
                       ['--type',       '-t', GetoptLong::OPTIONAL_ARGUMENT],
                       ['--crc',        '-c', GetoptLong::NO_ARGUMENT],
                       ['--end',        '-e', GetoptLong::NO_ARGUMENT],
                       ['--no-extract', '-n', GetoptLong::NO_ARGUMENT],                
                       ['--unknown',    '-u', GetoptLong::NO_ARGUMENT])

 options = {}
 regtype=/./

 opts.each do |opt, arg|
   case opt
    when '--help'
      options[:help] = true
    when '--directory'
      options[:directory] = arg
    when '--type'
      if arg.size == 4 then
	eval "regtype = /#{arg}/"
      else
	eval "regtype = #{arg}"
      end
      options[:type]=true
    when '--crc'
      options[:crc] = true
    when '--no-extract'
      options[:'no-extract'] = true
    when '--end'
      options[:end] = true
    when '--unknown'
      options[:unknown] = true
    else
      raise "Invalid option '#{opt}'."
    end
   end
rescue
   puts "Error: #{$!}"
   puts USAGE_SHORT
   exit 1
end

dir="extracted"
if options[:directory] then
  dir=options[:directory]
end
dir=dir.chomp("/")

fname=ARGV.shift

if options[:help] == true then
      puts USAGE
      exit 0
elsif not fname then
      puts USAGE_SHORT
      exit 1
end

while fname do      
    chunks = []
    begin
      io = File.open(fname) 
      head=PngHeader.read(io).header
      if head!="\x89PNG\r\n\x1A\n" then 
	puts "#{fname} is not PNG-image"
	exit 1
      end
    rescue
	puts "#{$!}"
	fname=ARGV.shift
	next
    end
    begin
      begin
	  chunk = PngChunk.read(io)
	  chunks << chunk
      end until chunk.type == "IEND"
    rescue
      io.close
      puts "Error: #{$!}"
      exit 1
    end
    fulldir=dir+"/"+fname
    if not File.exists? fulldir then
      begin
	FileUtils.mkpath(fulldir)
      rescue
	puts "#{$!}" 
	exit 1
      end
    elsif (File.exists? fulldir) && (File.directory? fulldir)==false then
      puts "#{fulldir}: is not directory"
      exit 1
    end
    i=0
    puts "Processing file '#{fname}'..."
    chunks.each do |chunk|
      if chunk.type=~regtype then
        filename = fulldir + "/" + chunk.type + i.to_s# + "-" + chunk.checksum.to_s
	fileout=filename
	if (fileout.length > 30)==true then
	  fileout=dir+"/"+"(filename)/" + chunk.type + i.to_s
	end
	if options[:crc]==true then
	 crc=(Zlib::crc32(chunk.type+chunk.data)==chunk.checksum)
	 if not crc then
	   if options[:'no-extract']==true then
	      printf "%-8s  CRC32:%-8s\n", chunk.type+i.to_s, crc.to_s
	   else
	      printf "  * Created  %-30s  CRC32:%-8s  \n", fileout, crc.to_s
	      extract_chunk(chunk, i, fulldir)
	   end
	   i+=1
	   next
	 end	
	end
	if options[:unknown]==true then
	 if not chunk_types.include?(chunk.type) then
	   if options[:'no-extract']==true then
	      printf "%-8s  Unknown chunk: %s\n", chunk.type+i.to_s, chunk.type
	   else	   
	      printf "  * Created  %-30s  Unknown chunk: %s\n", fileout, chunk.type 
	      extract_chunk(chunk, i, fulldir)
	   end
	   i+=1
	   next
	 end
	end
	if (not options[:end]==true) && (not options[:unknown]==true) && (not options[:crc]==true) then
	 if chunk_types.include?(chunk.type) then
	   suff=""
	 else
	   suff="Unknown chunk: #{chunk.type}"
	 end
	 i+=1
	 crc=(Zlib::crc32(chunk.type+chunk.data)==chunk.checksum)
	 if options[:'no-extract']==true then
	   printf "%-8s  CRC32:%-8s  #{suff}\n", chunk.type+i.to_s, crc.to_s
	 else
	   printf "  * Created  %-30s  CRC32:%-8s  #{suff}\n", fileout, crc.to_s   
	   extract_chunk(chunk, i, fulldir)
	 end
	end
      end
    end
    suffix = io.read
    io.close
    if (options[:end]==true) || ( (not options[:type]==true) && (not options[:unknown]==true) && (not options[:crc]==true) ) then
      if suffix!='' then
       if options[:'no-extract']==true then
	  puts "Attention! File #{fname} has data next for IEND chunk:\n#{suffix}\n"
       else
	  puts "Attention! File #{fname} has data next for IEND chunk\nWritten to #{fulldir}/suffix.txt"
	  f = open(fulldir+"/suffix.txt", 'wb')
	  f.write(suffix)
	  f.close
       end
      end
    end
 fname=ARGV.shift
end 
